import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './../../screens/login'
import Profile from './../../screens/profile'
import editProfile from './../../screens/editprofile'
import Friend from './../../screens/friend'
const stackConfig = {
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            header: null
        }
    },
    editProfile: {
        screen: editProfile,
        navigationOptions: {
            header: null
        }
    },
    Friend: {
        screen: Friend,
        navigationOptions: {
            header: null
        }
    },
}

const AppNavigator = createStackNavigator(stackConfig, {
    initialRouteName: 'Profile'
})
const AppContainer = createAppContainer(AppNavigator)

export default AppContainer