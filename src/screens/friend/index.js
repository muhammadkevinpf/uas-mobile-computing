import React, { Component } from 'react'
import { View, Text, ActivityIndicator, TouchableOpacity, FlatList } from 'react-native'
import { Button, Input, Item } from 'native-base'
import styles from './styles'
// import { FlatList } from 'react-native-gesture-handler';

class Friend extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            dataSource: []
        }
    }
    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson
                })
            })
    }
    _renderItem = ({ item }) => (
        <TouchableOpacity>
            <Text>{item.title}</Text>
        </TouchableOpacity>
    );
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ height: 667, backgroundColor: "#192879" }}>
                    <View style={styles.mainBody}>
                        <ActivityIndicator size="large" animating />
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 667, backgroundColor: "#192879" }}>
                    <View style={styles.mainBody}>
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => index}
                        />
                    </View>
                </View>
            )
        }
    }
}

export default Friend