const styles = {
    mainBody: {
        marginTop: 30,
        marginLeft: 24,
        marginRight: 25,
        marginBottom: 70,
    },
    imgLogin: {
        width: 190,
        height: 230,
        marginLeft: 80,
        marginTop: 40
    },
    inputuser: {
        width: 300,
        height: 50,
        borderColor: "#43519D",
        backgroundColor: "#283786",
        borderRadius: 8
    },
    labeluser: {
        fontSize: 20,
        color: "#414E93",
        marginBottom: 8
    },
    buttonSignIn: {
        width: 300,
        height: 50,
        marginTop: 25,
        backgroundColor: "#50d9ea"
    },
    labelButton: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold",
    }
}

export default styles