import React, { Component } from 'react'
import { View, Text, Image, Alert } from 'react-native'
import { Button, Input, Item } from 'native-base'
import styles from './styles'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            navigation: this.props.navigation
        }
    }
    myValidate = () => {
        const { username, password } = this.state;
        if (username == "" && password == "") {
            Alert.alert('Please fill username and password')
        } else if (username == "lul" && password == "edan") {
            this.props.navigation.navigate("Profile")
            // Alert.alert('edan')
        } else {
            Alert.alert('Data not found')
        }
    }
    render() {
        return (
            <View style={{ height: 667, backgroundColor: "#192879" }}>
                <View style={styles.mainBody}>
                    <Image style={styles.imgLogin} source={require("../../assets/shield.png")} />
                    <View style={{ marginBottom: 16 }}>
                        <Text style={styles.labeluser}>
                            Username
                        </Text>
                        <Item regular style={styles.inputuser}>
                            <Input style={{ color: "#fff" }} autoCapitalize="none" onChangeText={username => this.setState({ username })} />
                        </Item>
                    </View>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={styles.labeluser}>
                            Password
                    </Text>
                        <Item regular style={styles.inputuser}>
                            <Input style={{ color: "#fff" }} autoCapitalize="none" secureTextEntry={true} onChangeText={password => this.setState({ password })} />
                        </Item>
                    </View>
                    <Button block style={styles.buttonSignIn} onPress={this.myValidate}>
                        <Text style={styles.labelButton}>
                            Sign In
                        </Text>
                    </Button>
                </View>
            </View>
        )
    }
}

export default Login