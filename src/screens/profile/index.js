import React, { Component } from 'react'
import { View, Text, Image, Alert, AsyncStorage } from 'react-native'
import { Button, Input, Item } from 'native-base'
import styles from './styles'

class Profile extends Component {

    logoutProfile = () => {
        this.props.navigation.navigate("Login")
    }

    componentDidMount = () => {
        AsyncStorage.getItem('username').then((value) => this.setState({ 'username': value }));
        AsyncStorage.getItem('fullname').then((value) => this.setState({ 'fullname': value }));
    }
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            fullname: "",
            navigation: this.props.navigation
        }
    }

    render() {
        return (
            <View style={{ height: 667, backgroundColor: "#192879" }}>
                <View style={styles.mainBody}>
                    <Image style={styles.imgProfile} source={require("../../assets/poto150.jpg")} />
                    <Text style={styles.name}>
                        {this.state.fullname}
                    </Text>
                    <Text style={styles.username}>
                        @{this.state.username}
                    </Text>

                    <View style={styles.itemprofile}>
                        <Image style={styles.imgitem} source={require("../../assets/iccoffee.png")} />
                        <Text style={styles.labelitem}>
                            Redeem Coffee
                        </Text>
                        <Text style={styles.sublabelitem}>
                            Available
                        </Text>
                        <Button block style={styles.btnSmall}>
                            <Text style={styles.labelButtonSmall}>
                                Redeem
                            </Text>
                        </Button>
                    </View>

                    <View style={styles.itemprofile}>
                        <Image style={styles.imgitem} source={require("../../assets/icphone.png")} />
                        <Text style={styles.labelitem}>
                            Edit Profile
                        </Text>
                        <Text style={styles.sublabelitem}>

                        </Text>
                        <Button block style={styles.btnSmall} onPress={() => this.props.navigation.navigate("editProfile")}>
                            <Text style={styles.labelButtonSmall}>
                                Edit
                            </Text>
                        </Button>
                    </View>

                    <View style={styles.itemprofile}>
                        <Image style={styles.imgitem} source={require("../../assets/icbadge.png")} />
                        <Text style={styles.labelitem}>
                            Friend List
                        </Text>
                        <Text style={styles.sublabelitem}>
                            180 Friends
                        </Text>
                    </View>

                    <Button block style={styles.buttonsignout} onPress={this.logoutProfile}>
                        <Text style={styles.labelButton}>
                            Sign Out
                        </Text>
                    </Button>
                </View>
            </View>
        )
    }
}

export default Profile