const styles = {
    mainBody: {
        marginTop: 30,
        marginLeft: 24,
        marginRight: 25,
        marginBottom: 70,
    },
    imgProfile: {
        marginLeft: 100,
        marginTop: 50,
        height: 120,
        width: 120,
        borderRadius: 80
    },
    name: {
        color: "white",
        fontSize: 22,
        marginLeft: 70,
        marginTop: 22
    },
    username: {
        color: "#626fb4",
        fontSize: 16,
        marginLeft: 110,
        marginTop: 4
    },
    itemprofile: {
        marginTop: 30,
    },
    labelitem: {
        marginTop: -45,
        marginLeft: 60,
        fontSize: 18,
        color: "white"
    },
    sublabelitem: {
        marginLeft: 60,
        fontSize: 16,
        color: "#509dea"
    },
    buttonsignout: {
        width: 300,
        height: 50,
        marginTop: 25,
        backgroundColor: "#50d9ea"
    },
    labelButton: {
        color: "#fff",
        fontSize: 16,
    },
    btnSmall: {
        width: 80,
        height: 30,
        backgroundColor: "#50d9ea",
        position: "absolute",
        right: 10,
        top: 15
    },
    labelButtonSmall: {
        color: "#fff",
        fontSize: 13,
    }
}

export default styles