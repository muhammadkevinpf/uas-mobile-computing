const styles = {
    mainBody: {
        marginTop: 30,
        marginLeft: 24,
        marginRight: 25,
        marginBottom: 70,
    },
    imgLogin: {
        width: 190,
        height: 230,
        marginLeft: 80,
        marginTop: 40
    },
    name: {
        color: "white",
        fontSize: 22,
        marginLeft: 100,
        marginTop: 22,
        marginBottom: 30
    },
    inputuser: {
        width: 300,
        height: 50,
        borderColor: "#43519D",
        backgroundColor: "#283786",
        borderRadius: 8
    },
    labeluser: {
        fontSize: 20,
        color: "#414E93",
        marginBottom: 8
    },
    buttonSignIn: {
        width: 300,
        height: 50,
        marginTop: 25,
        backgroundColor: "#50d9ea"
    },
    buttonCancel: {
        width: 300,
        height: 50,
        marginTop: 25,
        backgroundColor: "rgba(160, 196, 255,0.5)"
    },
    labelButton: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold",
    }
}

export default styles