import React, { Component } from 'react'
import { View, Text, Image, Alert, AsyncStorage } from 'react-native'
import { Button, Input, Item } from 'native-base'
import styles from './styles'

class Editprofile extends Component {
    componentDidMount = () => {
        AsyncStorage.getItem('username').then((value) => this.setState({ 'username': value }));
        AsyncStorage.getItem('fullname').then((value) => this.setState({ 'fullname': value }));
        AsyncStorage.getItem('password').then((value) => this.setState({ 'password': value }));
    }
    saveEditProfile = () =>{
        Alert.alert("Data Saved");
    }
    setUsername = (value) => {
        AsyncStorage.setItem('username', value);
        this.setState({ 'username': value });
    }
    setFullname = (value) => {
        AsyncStorage.setItem('fullname', value);
        this.setState({ 'fullname': value });
    }
    setPassword = (value) => {
        AsyncStorage.setItem('password', value);
        this.setState({ 'password': value });
    }
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            fullname: "",
            navigation: this.props.navigation
        }
    }

    render() {
        return (
            <View style={{ height: 667, backgroundColor: "#192879" }}>
                <View style={styles.mainBody}>
                    <Text style={styles.name}>
                        Edit Profile
                    </Text>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={styles.labeluser}>
                            Full Name
                        </Text>
                        <Item regular style={styles.inputuser}>
                            <Input style={{ color: "#fff" }} autoCapitalize="none" onChangeText={this.setFullname} value={this.state.fullname} />
                        </Item>
                    </View>

                    <View style={{ marginBottom: 16 }}>
                        <Text style={styles.labeluser}>
                            Username
                        </Text>
                        <Item regular style={styles.inputuser}>
                            <Input style={{ color: "#fff" }} autoCapitalize="none" onChangeText={this.setUsername} value={this.state.username} />
                        </Item>
                    </View>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={styles.labeluser}>
                            Password
                    </Text>
                        <Item regular style={styles.inputuser}>
                            <Input style={{ color: "#fff" }} autoCapitalize="none" secureTextEntry={true} onChangeText={this.setPassword} value={this.state.password} />
                        </Item>
                    </View>
                    <Button block style={styles.buttonSignIn} onPress={this.saveEditProfile}>
                        <Text style={styles.labelButton}>
                            Save
                        </Text>
                    </Button>
                    <Button onPress={() =>this.props.navigation.navigate("Profile")} block style={styles.buttonCancel}>
                        <Text style={styles.labelButton}>
                            Cancel
                        </Text>
                    </Button>
                </View>
            </View>
        )
    }
}

export default Editprofile